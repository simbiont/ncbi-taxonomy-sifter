# ncbi-taxonomist-sifter.py

## Summary
Reads a file containing taxonomy related results, e.g. blast results, and keep or removes results with a given taxid.

### Limitations
- NCBI taxonomies only (so far)
- Only results with one entry per line, e.g. blast `-outfmt 6`

### Requirements

  - TaxonomyDatabase from ncbi-simbiont.py [ncbi-taxonomist.py]
  - A file with taxids you want to either remove or keep.
    One taxid per line.


## Examples
Simple examples to setup and run `ncbi-taxonomist-sifter`.

Variable names, e.g. `$TS`, refer to paths.
If you cloned `ncbi-taxonomist-sifter` in the directory
`some/path/in/system`, `$TS` will refer to `some/path/in/system/ncbi-taxonomist-sifter` during the example.

#### Setup

0.  clone [ncbi-taxonomist.py]:
    `$TX`: path to `ncbi-taxonomist`

1. clone `ncbi-taxonomist-sifter`

    `$TS`: path to `ncbi-taxonomist-sifter`

2. Create and enter a working directory `$WD` (e.g. `taxowd`):
     - `mkdir $WD`
     - `cd $WD`

3. Prepare TaxonomyDb using [ncbi-taxonomist.py]
    - `$TX/src/ncbi.taxonomist.py --ids $TS/example/taxids.db -e you@email.host -db $WD/taxonomy.db --cacheonly`

#### Parameters used in examples:
The example uses a blast output ('blast.output') generated using the`-outfmt 6` parameter, i.e. each result is on one line, usually tab-separated. For educative reasons, the column separator was changed to '@' in the example file.
The results are read from standard input. `ncbi-taxonomist-sifter` counts
columns 0-based, i.e. the first column is 0. Thanks to Dr. Joanna Cobbin for the
example blast output file.

argument      | Meaning                              | example value
--------------|--------------------------------------|--------------
`--taxids`    | File with one NCBI taxid per line    | `taxid.list`
`--separator` | Column separator in input file       | `@`
`--localdb`   | `ncbi-taxonomist` taxonomy database  | `taxonomy.db`
`--column`    | 0-based column number with taxid     | `7`
`--outfile`   | output file, STDOUT if not given     | `outfile`
`--keep`      | keep taxids. Without --keep, remove  | `--keep`

  
#### Example 1: Keep entries with specific taxids

    `$TS/src/ncbi-taxonomist-sifter.py -k -i $TS/example/taxid.list -c 7 -db $WD/taxonomy.db -t '@' -o $WD/outfile < $TS/example/blast.output`


#### Example 2: Remove entries with specific taxids and write to STDOUT

    `$TS/src/ncbi-taxonomist-sifter.py -i $TS/example/taxid.list -c 7 -db $WD/taxonomy.db -t '@' < $TS/example/blast.output`

[ncbi-taxonomist.py]: (https://gitlab.com/simbiont/ncbi-taxonomist.git)