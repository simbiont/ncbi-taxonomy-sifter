#  -----------------------------------------------------------------------------
#  File: cladistic.py
#  Author: Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  Copyright: 2018 The University of Sydney
#  Version: 0
#  Description:
#  -----------------------------------------------------------------------------
from clade import basic_clade
from clade import virus_clade

class Cladistic:

  eukaryota_ranks = ['superkingdom', 'kingdom', 'phylum', 'subphylum', 'class',
                     'superorder', 'order', 'suborder', 'infraorder',
                     'parvorder', 'superfamily', 'family', 'subfamily', 'genus',
                     'subgenus', 'species', 'subspecies']

  bacteria_ranks = ['superkingdom', 'phylum', 'class', 'order', 'family',
                    'genus', 'species']

  viroid_ranks = ['superkingdom', 'family', 'genus', 'species']

  virus_ranks = ['superkingdom', 'group', 'order', 'family', 'subfamily',
                 'genus', 'species']
  unknown_ranks = ['superkingdom', 'subspecies', 'species']
  ranks = {'bacteria' : bacteria_ranks, 'archaea' : bacteria_ranks,
           'eukaryota' : eukaryota_ranks, 'viroids' : viroid_ranks,
           'unknown' : unknown_ranks, 'viruses': virus_ranks}

  clades = {}
  def __init__(self):
    for i in Cladistic.ranks:
      if i == 'viruses':
        Cladistic.clades[i] = virus_clade.VirusClade(Cladistic.ranks[i])
      else:
        Cladistic.clades[i] = basic_clade.BasicClade(i, Cladistic.ranks[i])

  def cladeExists(self, cladename):
    return cladename in Cladistic.clades

  def identify_clade(self, taxa):
    for i in taxa:
      if i.rank.lower() == 'superkingdom':
        return i.name.lower()
    return 'unknown'
