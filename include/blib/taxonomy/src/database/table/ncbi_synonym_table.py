#!/usr/bin/env python3
#  -------------------------------------------------------------------------------
#  \file ncbi_synonym_table.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2018 The University of Sydney
#  \version 0.1.0
#  \description
#  -------------------------------------------------------------------------------

from . import taxonomy_table

class NcbiSynonymTable(taxonomy_table.TaxonomyTable):

  def __init__(self, name, database):
    super().__init__(name=name, database=database)

  def create(self):
    c = self.database.conn.cursor()
    stmt = """CREATE TABLE IF NOT EXISTS {0}
              (
                id           INTEGER PRIMARY KEY,
                taxid        INT,
                synonym      TEXT NULL,
                FOREIGN KEY (taxid) REFERENCES taxa(taxid)
              )""".format(self.name)
    c.execute(stmt)
    return self

  def get_rows(self):
    c = self.database.conn.cursor()
    return c.execute("SELECT taxid, synonym FROM {0}".format(self.name))

  def insert(self, taxa):
    c = self.database.conn.cursor()
    stmt = """INSERT OR IGNORE INTO {0} (taxid, synonym) VALUES (?,?)""".format(self.name)
    values = []
    for i in taxa:
      if len(i.synonyms) > 0:
        for j in i.synonyms:
          values.append([i.taxid, j])
    c.executemany(stmt, values)
    self.database.conn.commit()
    self.count_rows()
