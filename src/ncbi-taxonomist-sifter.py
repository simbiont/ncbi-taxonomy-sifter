#!/usr/bin/env python3
#-------------------------------------------------------------------------------
#  \file ncbi-taxonomist-sifter.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2018 The University of Sydney
#  \version 0.2.0
#  \description Filter input files for wanted or unwanted NCBI taxonomies.
#               It expects an SQLite3 TaxonomDb [0] for the lookups.
#
#  \references [0]: https://gitlab.com/simbiont/ncbi-taxonomist.git
#-------------------------------------------------------------------------------

import os
import io
import sys
import argparse

sys.path.insert(1, os.path.join(sys.path[0], '../include/blib/taxonomy/src'))
import taxonomist

class DocumentReader:

  def __init__(self):
    pass

  def read(self):
    raise NotImplementedError("Help! I'm virtual and require an implementation")

class TaxonomySifter:

  def __init__(self, taxonomydb_path):
    self.taxonomist = taxonomist.NcbiTaxonomist()
    self.localdb = self.taxonomist.connect_local_db(taxonomydb_path)
    self.taxonomist.read_local_database(self.localdb)
    self.keep = True
    self.sieve = {}

  def set_options(self, taxids, keep):
    self.keep = keep
    self.prepare_sieve(taxids)
    print("Sieve size: {}".format(len(self.sieve)), file=sys.stderr)
    if self.keep == True:
      print("Mode: Keep taxids in sieve", file=sys.stderr)
    else:
      print("Mode: remove taxids in sieve", file=sys.stderr)

  def prepare_sieve(self, taxids):
    fh = open(taxids, 'r')
    for i in fh:
      if int(i.rstrip()) in self.taxonomist.taxa:
        self.sieve[int(i.rstrip())] = 0
    fh.close()

  def keepEntry(self, taxid):
    if self.keep:
      if taxid in self.sieve:
        return True
      return False
    if not self.keep:
      if taxid in self.sieve:
        return False
    return True

class LineReader(DocumentReader):

  def __init__(self):
    super().__init__()
    self.expected_columns = 0
    self.outfile = None
    self.taxonomist = taxonomist.NcbiTaxonomist()

  def sift(self, taxsifter, colnum, addLineage, colsep='\t', outfile=None):
    self.set_output(outfile)
    isFirstLine = True
    line_num = 0
    for i in sys.stdin:
      line_num += 1
      cols = i.rstrip().split(colsep)
      if isFirstLine:
        self.expected_columns = len(cols)
        if self.isBadColnumber(colnum):
          sys.exit("Bad column number. Aborting.")
        isFirstLine = False
      if not self.isValidLine(line_num, len(cols)):
        sys.exit("Error in colum numbers. Aborting.")
      if taxsifter.keepEntry(int(cols[colnum])):
        self.write_results_file_lineage(cols, colsep, int(cols[colnum]), addLineage)
    self.close_outfile()

  def isBadColnumber(self, colnum):
    if colnum > self.expected_columns:
      print("Error: given column number is larger than columns in result")
      return True
    return False

  def set_output(self, outfile):
    if outfile == None:
      print("Writing results  to STDOUT", file=sys.stderr)
    else:
      print("Writing results  to {}".format(outfile), file=sys.stderr)
      self.outfile = open(outfile, 'w')

  def close_outfile(self):
    if self.outfile != None:
      self.outfile.close()

  def write_results_file_lineage(self, cols, colsep, taxid, addLineage):
    if addLineage:
      lin = self.taxonomist.get_normalized_lineage(self.taxonomist.assemble_lineage_from_taxid(taxid))
      cols += [x.name for x in lin]
    line = colsep.join(x for x in cols)
    self.write_result(line)

  def write_result(self, line):
    if self.outfile == None:
      print(line.rstrip())
    else:
      self.outfile.write(line)

  def isValidLine(self, line_num, linecols):
    if linecols != self.expected_columns:
      print("Error: Line {} has {} columns. Expecting {}.".format(line_num, linecols,
                                                                  self.expected_columns))
    return True

def main():
  ap = argparse.ArgumentParser(description='Sieve NCBI taxonomies from result files')
  ap.add_argument('-i', '--taxids',
                  type=str,
                  required=True,
                  help='File with NCBI taxid, one per line.')
  ap.add_argument('-k', '--keep',
                  action='store_true',
                  default = False,
                  help='Keep only entries with taxids given in --taxids. If not given, negated ')
  ap.add_argument('-c', '--column',
                  type=int,
                  required=True,
                  help='Result file is one entry per line and taxid is in column number COLUMN (0-based)')
  ap.add_argument('-o', '--outfile',
                  type=str,
                  default=None,
                  help='Print to OUTFILE, Default: STDOUT')
  ap.add_argument('-db', '--localdb',
                  type=str,
                  help='Local SQLite TaxonomyDB for lookups.')
  ap.add_argument('-t', '--separator',
                  type=str,
                  default='\t',
                  help='Column separator. Default: \\t')
  ap.add_argument('--add_lineage',
                  action='store_true',
                  help='Add lineage after taxid')

  args = ap.parse_args()
  ts = TaxonomySifter(args.localdb)
  ts.set_options(args.taxids, args.keep)
  r = LineReader()
  r.sift(ts, args.column, args.add_lineage, args.separator, args.outfile)

  return 0

if __name__ == '__main__':
  main()
